import socket
import picar_4wd as fc

HOST = "192.168.1.10" # IP address of your Raspberry PI
PORT = 65432          # Port to listen on (non-privileged ports are > 1023)
power_val = 50

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen()
    
    try:
        while 1:
            client, clientInfo = s.accept()
            #print("server recv from: ", clientInfo)
            data = client.recv(1024)      # receive 1024 Bytes of message in binary format
            #print(data)
            dist = fc.get_distance_at(0)
            if dist != -2 and dist < 20:
                if data == b"forward\r\n":
                    fc.forward(power_val)
                    client.sendall(b"forwardyes\r\n")
                elif data == b"backward\r\n":
                    fc.backward(power_val)
                    client.sendall(b"backwardyes\r\n")
                elif data == b"left\r\n":
                    fc.turn_left(power_val)
                    client.sendall(b"leftyes\r\n")
                elif data == b"right\r\n":
                    fc.turn_right(power_val)
                    client.sendall(b"rightyes\r\n")
                else:
                    fc.stop()
                    client.sendall(b"yes\r\n")
            else:
                if data == b"forward\r\n":
                    fc.forward(power_val)
                elif data == b"backward\r\n":
                    fc.backward(power_val)
                elif data == b"left\r\n":
                    fc.turn_left(power_val)
                elif data == b"right\r\n":
                    fc.turn_right(power_val)
                else:
                    fc.stop()
                client.sendall(data) # Echo back to client
            
    except: 
        print("Closing socket")
        fc.stop()
        client.close()
        s.close()    