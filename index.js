var server_port = 65432;
var server_addr = "192.168.1.10";   // the IP address of your Raspberry PI

function client(direction){
    
    const net = require('net');
    //var input = document.getElementById("myName").value;

    const client = net.createConnection({ port: server_port, host: server_addr }, () => {
        // 'connect' listener.
        console.log('connected to server!');
        // send the message
        client.write(`${direction}\r\n`);
    });
    
    // get the data from the server
    client.on('data', (data) => {
        document.getElementById("greet_from_server").innerHTML = "Moving: no";
        document.getElementById("greet").innerHTML = "Turning: no";
        document.getElementById("front_detect").innerHTML = "Object detected: no";
        if (data == `${"left"}\r\n` || data == `${"right"}\r\n`) {
            document.getElementById("greet").innerHTML = "Turning: " + data;
        } else if (data == `${"forward"}\r\n` || data == `${"backward"}\r\n`) {
            document.getElementById("greet_from_server").innerHTML = "Moving: " + data;
        } else if (data == `${"yes"}\r\n`) {
            document.getElementById("front_detect").innerHTML = "Object detected: yes";
        } else if (data == `${"forwardyes"}\r\n`) {
            document.getElementById("front_detect").innerHTML = "Object detected: yes";
            document.getElementById("greet_from_server").innerHTML = "Moving: forward";
        } else if (data == `${"backwardyes"}\r\n`) {
            document.getElementById("front_detect").innerHTML = "Object detected: yes";
            document.getElementById("greet_from_server").innerHTML = "Moving: backward";
        } else if (data == `${"leftyes"}\r\n`) {
            document.getElementById("front_detect").innerHTML = "Object detected: yes";
            document.getElementById("greet").innerHTML = "Turning: left";
        } else if (data == `${"rightyes"}\r\n`) {
            document.getElementById("front_detect").innerHTML = "Object detected: yes";
            document.getElementById("greet").innerHTML = "Turning: right";
        }
        console.log(data.toString());
        client.end();
        client.destroy();
    });

    client.on('end', () => {
        console.log('disconnected from server');
    });


}

function greeting(direction){

    // get the element from html
    //var name = document.getElementById("myName").value;
    // update the content in html
    //document.getElementById("greet").innerHTML = "Hello " + name + " !";
    // send the data to the server 
    //to_server(name);
    client(direction);

}
